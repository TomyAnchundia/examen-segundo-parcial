// Crear una función JavaScript para guardar los datos 
// de estudiantes (Cédula, apellidos, nombres, dirección, 
//     semestre, paralelo, correo electrónico) usando PL/SQL. 
//     Validar los datos antes de guardarlos. 

const pool = require("../dbStudent");
//debemos tener previamente conexion a la base de datos 
//donde queremos almacenar  los estudiantes, en caso de tener la conexion
// usamos la extension de pool que nos permite comunicacion e insersion de 
//plsql dentro de una funcion de js

const createStudent = async (req, res ) => {
    try {
      const {
        cedula,
        apellidos,
        nombres,
        direccion,
        semestre,
        paralelo, 
        correo_electronico
        
      } = req.body;
  
      const result = await pool.query(
        "INSERT INTO student (cedula, apellidos, nombres, direccion, semestre, paralelo, correo_electronico) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING*",
        [
          cedula,
          apellidos,
          nombres,
          direccion,
          semestre, 
          paralelo, 
          correo_electronico,
        ]
      );
  
      res.json(result.rows[0]);
    } catch (error) {
      messages.error('No se pudo guardar los datos del estudiante')
    }
  };


  module.exports = {
    createStudent
  };